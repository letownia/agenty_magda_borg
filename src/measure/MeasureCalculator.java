package measure;

import jade.core.AID;

import java.io.Serializable;
import java.util.AbstractMap;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import algorithm.Schedule;
import dtp.commission.Commission;
import dtp.jade.algorithm.agent.AlgorithmAgentParent;
import dtp.simmulation.SimInfo;

/**
 * Base class for all other calculators which calculate specific measure. To add
 * new MeasureCalculator (and use it), you have to: 1. create your class which
 * extends MeasureCalculator 2. It has to be in measure package 3. Now you can
 * use it in configuration (you using class name)
 */
public abstract class MeasureCalculator implements Serializable {
	protected Boolean useOldCommissions = true;
	protected Boolean returnMin = false;
	protected Boolean onlyUndeliveredCommissions = true;
	private static final long serialVersionUID = 5357884320339593987L;
	protected SimInfo info;
	protected int timestamp;
	protected List<Commission> commissions;

	/**
	 * @param oldSchedules
	 *            - schedules before ST
	 * @param newSchedules
	 *            - schedules after ST
	 * @return
	 */
	public abstract Measure calculateMeasure(Map<AID, Schedule> oldSchedules,
			Map<AID, Schedule> newSchedules, AlgorithmAgentParent agent);

	public abstract String getName();

	public void setSimInfo(SimInfo info) {
		this.info = info;
	}

	public void setTimestamp(int timestamp) {
		this.timestamp = timestamp;
	}

	public void setCommissions(List<Commission> commissions) {
		this.commissions = commissions;
	}
	public Commission calculateWorstOffenderForHolon(Schedule schedule)
	{
		if(schedule == null)
			while(true)
				System.out.println("FUCK ERROR calculateWorstOffenderForHolon schedule null");
		
		Map<AID, Schedule> hackedMap = new HashMap<AID,Schedule>();
		List<Commission> allCommissions;
		if(onlyUndeliveredCommissions)
			allCommissions  = schedule.getUndeliveredCommissions(info.getDepot(), timestamp);//schedule.getAllCommissions();
		else
			allCommissions = schedule.getAllCommissions();
		//A schedule has old commmission also I believe? Might be good to add a bool for including/excluding oldCommissions
		for(Commission c : allCommissions){
			//Or could use "GraphSchedule"
			AID fakeAID = new AID();
			fakeAID.setLocalName(Integer.toString(c.getID()));
			
			Schedule n = Schedule.copy(schedule);
			n.removeCommission(c);
			hackedMap.put(fakeAID, n);
		}
		Measure lookForWorst;
		if(this.useOldCommissions)
			lookForWorst = calculateMeasure(hackedMap, null, null);
		else
			lookForWorst = calculateMeasure(null,hackedMap, null);
		
		Map<String, Double> values = lookForWorst.getValues();
		System.out.println("values . len = "
				+ values.size());
		Entry<String, Double> biggest = new AbstractMap.SimpleEntry<String, Double>("none", Double.NEGATIVE_INFINITY);
		Entry<String, Double> smallest = new AbstractMap.SimpleEntry<String, Double>("none", Double.POSITIVE_INFINITY);
		for(Entry<String, Double> e : values.entrySet() ){
			System.out.println("Removing " + e.getKey() + " would have cost " +e.getValue());
			if(e.getValue() > biggest.getValue())
				biggest = e;
			if(e.getValue() < smallest.getValue())
				smallest = e;
		}
		Commission smallestCommission = null;
		Commission biggestCommission = null;
		for(Commission c : allCommissions){
			if(Integer.toString(c.getID()).equals(biggest.getKey()))
				biggestCommission = c;
			if(Integer.toString(c.getID()).equals(smallest.getKey()))
				smallestCommission = c;
		}
		if(returnMin)
			return smallestCommission;
		else
			return biggestCommission;
	}
}
